import java.util.Scanner;

public class Shop {

  public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);

    Printer[] printers = new Printer[4];

    for (int i = 0; i < 1; i++) {

      printers[i] = new Printer();

      // BRAND
      System.out.println("Please enter the brand: ");
      printers[i].brand = sc.nextLine();

      // isWIFI
      System.out.println("Is it WiFi? (True or False): ");
      printers[i].isWifi = Boolean.parseBoolean(sc.nextLine());

      // PRINT SPEED
      System.out.println("Please enter the Print Speed: ");
      printers[i].printSpeed = Double.parseDouble(sc.nextLine());

    }

    System.out.println("Brand: "+ printers[0].brand);
    System.out.println("isWifi: "+ printers[0].isWifi);
    System.out.println("Print Speed: "+ printers[0].printSpeed);

    System.out.println("How many pages dp you want to print?");
    printers[0].print(sc.nextInt());

  }

}
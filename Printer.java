public class Printer {

    public double printSpeed; // pages per minute (ppm)
    public String brand;
    public boolean isWifi;

    public void print(int numPages) {

      System.out.println("Printing " + numPages + " pages...");

      System.out.println("Process took " + Math.ceiling(numPages/this.printSpeed) + " minutes.");
      
    }

}